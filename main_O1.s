	.file	"main.c"
	.text
	.align	2
	.globl	mm_mul
	.type	mm_mul, @function
mm_mul:
.LFB0 = .
	.cfi_startproc
	ble	$r7,$r0,.L1
	or	$r18,$r6,$r0
	or	$r19,$r4,$r0
	addi.w	$r12,$r7,-1
	bstrpick.d	$r12,$r12,31,0
	alsl.d	$r12,$r12,$r0,3
	addi.d	$r6,$r6,8
	add.d	$r6,$r12,$r6
	addi.w	$r4,$r8,-1
	bstrpick.d	$r4,$r4,31,0
	alsl.d	$r4,$r4,$r0,2
	addi.d	$r4,$r4,4
	addi.w	$r20,$r9,-1
	bstrpick.d	$r20,$r20,31,0
	alsl.d	$r20,$r20,$r0,2
	addi.d	$r20,$r20,4
	b	.L3
.L7:
	addi.d	$r17,$r17,4
	beq	$r17,$r4,.L5
.L8:
	or	$r16,$r5,$r0
	or	$r13,$r0,$r0
	ble	$r9,$r0,.L7
.L4:
	ld.d	$r12,$r18,0
	add.d	$r12,$r12,$r17
	ld.d	$r15,$r19,0
	add.d	$r15,$r15,$r13
	ld.d	$r14,$r16,0
	add.d	$r14,$r14,$r17
	fld.s	$f0,$r15,0
	fld.s	$f1,$r14,0
	fmul.s	$f1,$f0,$f1
	fld.s	$f0,$r12,0
	fsub.s	$f0,$f0,$f1
	fst.s	$f0,$r12,0
	addi.d	$r13,$r13,4
	addi.d	$r16,$r16,8
	bne	$r20,$r13,.L4
	b	.L7
.L5:
	addi.d	$r18,$r18,8
	addi.d	$r19,$r19,8
	beq	$r18,$r6,.L1
.L3:
	or	$r17,$r0,$r0
	bgt	$r8,$r0,.L8
	b	.L5
.L1:
	jr	$r1
	.cfi_endproc
.LFE0:
	.size	mm_mul, .-mm_mul
	.align	2
	.globl	main
	.type	main, @function
main:
.LFB1 = .
	.cfi_startproc
	or	$r4,$r0,$r0
	jr	$r1
	.cfi_endproc
.LFE1:
	.size	main, .-main
	.ident	"GCC: (Loongnix 8.3.0-6.lnd.vec.34) 8.3.0"
	.section	.note.GNU-stack,"",@progbits
