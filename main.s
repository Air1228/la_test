	.file	"main.c"
	.text
	.align	2
	.globl	mm_mul
	.type	mm_mul, @function
mm_mul:
.LFB0 = .
	.cfi_startproc
	addi.d	$r3,$r3,-80
	.cfi_def_cfa_offset 80
	st.d	$r22,$r3,72
	.cfi_offset 22, -8
	addi.d	$r22,$r3,80
	.cfi_def_cfa 22, 0
	st.d	$r4,$r22,-40
	st.d	$r5,$r22,-48
	st.d	$r6,$r22,-56
	or	$r14,$r7,$r0
	or	$r13,$r8,$r0
	or	$r12,$r9,$r0
	slli.w	$r14,$r14,0
	st.w	$r14,$r22,-60
	slli.w	$r13,$r13,0
	st.w	$r13,$r22,-64
	slli.w	$r12,$r12,0
	st.w	$r12,$r22,-68
	st.w	$r0,$r22,-20
	b	.L2
.L7:
	st.w	$r0,$r22,-24
	b	.L3
.L6:
	st.w	$r0,$r22,-28
	b	.L4
.L5:
	ld.w	$r12,$r22,-20
	slli.d	$r12,$r12,3
	ld.d	$r13,$r22,-56
	add.d	$r12,$r13,$r12
	ld.d	$r13,$r12,0
	ld.w	$r12,$r22,-24
	slli.d	$r12,$r12,2
	add.d	$r12,$r13,$r12
	fld.s	$f1,$r12,0
	ld.w	$r12,$r22,-20
	slli.d	$r12,$r12,3
	ld.d	$r13,$r22,-40
	add.d	$r12,$r13,$r12
	ld.d	$r13,$r12,0
	ld.w	$r12,$r22,-28
	slli.d	$r12,$r12,2
	add.d	$r12,$r13,$r12
	fld.s	$f2,$r12,0
	ld.w	$r12,$r22,-28
	slli.d	$r12,$r12,3
	ld.d	$r13,$r22,-48
	add.d	$r12,$r13,$r12
	ld.d	$r13,$r12,0
	ld.w	$r12,$r22,-24
	slli.d	$r12,$r12,2
	add.d	$r12,$r13,$r12
	fld.s	$f0,$r12,0
	fmul.s	$f0,$f2,$f0
	ld.w	$r12,$r22,-20
	slli.d	$r12,$r12,3
	ld.d	$r13,$r22,-56
	add.d	$r12,$r13,$r12
	ld.d	$r13,$r12,0
	ld.w	$r12,$r22,-24
	slli.d	$r12,$r12,2
	add.d	$r12,$r13,$r12
	fsub.s	$f0,$f1,$f0
	fst.s	$f0,$r12,0
	ld.w	$r12,$r22,-28
	addi.w	$r12,$r12,1
	st.w	$r12,$r22,-28
.L4:
	ld.w	$r13,$r22,-28
	ld.w	$r12,$r22,-68
	blt	$r13,$r12,.L5
	ld.w	$r12,$r22,-24
	addi.w	$r12,$r12,1
	st.w	$r12,$r22,-24
.L3:
	ld.w	$r13,$r22,-24
	ld.w	$r12,$r22,-64
	blt	$r13,$r12,.L6
	ld.w	$r12,$r22,-20
	addi.w	$r12,$r12,1
	st.w	$r12,$r22,-20
.L2:
	ld.w	$r13,$r22,-20
	ld.w	$r12,$r22,-60
	blt	$r13,$r12,.L7
	nop
	ld.d	$r22,$r3,72
	.cfi_restore 22
	addi.d	$r3,$r3,80
	.cfi_def_cfa_register 3
	jr	$r1
	.cfi_endproc
.LFE0:
	.size	mm_mul, .-mm_mul
	.align	2
	.globl	main
	.type	main, @function
main:
.LFB1 = .
	.cfi_startproc
	addi.d	$r3,$r3,-16
	.cfi_def_cfa_offset 16
	st.d	$r22,$r3,8
	.cfi_offset 22, -8
	addi.d	$r22,$r3,16
	.cfi_def_cfa 22, 0
	or	$r12,$r0,$r0
	or	$r4,$r12,$r0
	ld.d	$r22,$r3,8
	.cfi_restore 22
	addi.d	$r3,$r3,16
	.cfi_def_cfa_register 3
	jr	$r1
	.cfi_endproc
.LFE1:
	.size	main, .-main
	.ident	"GCC: (Loongnix 8.3.0-6.lnd.vec.34) 8.3.0"
	.section	.note.GNU-stack,"",@progbits
