	.file	"main.c"
	.text
	.align	2
	.align	4
	.globl	mm_mul
	.type	mm_mul, @function
mm_mul:
.LFB0 = .
	.cfi_startproc
	ble	$r7,$r0,.L1
	or	$r18,$r6,$r0
	addi.w	$r20,$r8,-1
	addi.w	$r6,$r7,-1
	addi.w	$r12,$r9,-1
	bstrpick.d	$r6,$r6,31,0
	alsl.d	$r6,$r6,$r0,3
	addi.d	$r13,$r18,8
	bstrpick.d	$r20,$r20,31,0
	alsl.d	$r20,$r20,$r0,2
	bstrpick.d	$r12,$r12,31,0
	alsl.d	$r12,$r12,$r0,2
	or	$r19,$r4,$r0
	add.d	$r6,$r6,$r13
	addi.d	$r20,$r20,4
	addi.d	$r4,$r12,4
	.align	4,54525952,4
.L3:
	or	$r15,$r0,$r0
	ble	$r8,$r0,.L5
	.align	4,54525952,4
.L8:
	ble	$r9,$r0,.L7
	ld.d	$r16,$r18,0
	ld.d	$r12,$r19,0
	or	$r14,$r5,$r0
	add.d	$r16,$r16,$r15
	fld.s	$f1,$r16,0
	add.d	$r17,$r4,$r12
	.align	4,54525952,4
.L4:
	ld.d	$r13,$r14,0
	fld.s	$f0,$r12,0
	addi.d	$r12,$r12,4
	add.d	$r13,$r13,$r15
	fld.s	$f2,$r13,0
	fneg.s	$f0,$f0
	addi.d	$r14,$r14,8
	fmadd.s	$f1,$f0,$f2,$f1
	fst.s	$f1,$r16,0
	bne	$r17,$r12,.L4
.L7:
	addi.d	$r15,$r15,4
	bne	$r15,$r20,.L8
.L5:
	addi.d	$r18,$r18,8
	addi.d	$r19,$r19,8
	bne	$r18,$r6,.L3
.L1:
	jr	$r1
	.cfi_endproc
.LFE0:
	.size	mm_mul, .-mm_mul
	.section	.text.startup,"ax",@progbits
	.align	2
	.align	4
	.globl	main
	.type	main, @function
main:
.LFB1 = .
	.cfi_startproc
	or	$r4,$r0,$r0
	jr	$r1
	.cfi_endproc
.LFE1:
	.size	main, .-main
	.ident	"GCC: (Loongnix 8.3.0-6.lnd.vec.34) 8.3.0"
	.section	.note.GNU-stack,"",@progbits
