void mm_mul(float** A, float** B, float** C, int m, int n, int k){
    int i, j, p;
    for(i = 0; i < m; i++){
        for(j = 0; j < n; j++){
            for(p = 0; p < k; p++){
                C[i][j] -= A[i][p] * B[p][j];
            }
        }
    }
}

int main(){
    return 0;
}
